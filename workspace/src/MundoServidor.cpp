// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
//Modificaciones por Alberto Gomez Jimenez
//Matricula 53980
//PRACTICA 1
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd);
	close(fd_sc);
	//close(fd_cs);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,350,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	 

	char str[] = "Jugador X anota su N gol"; 
	char fin[] = "Jugador X gana";
	const char bar = '\n';
	char mensaje_sc[200];
		
	//CAMBIO DE POSICIONES
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		jugador1.setYlimit();
		
		str[8] = '2';
		str[19] = (char)puntos2 + '0';
		for(int j = 0; j <= strlen(str); j++)
			write(fd, &str[j], 1);
		write(fd, &bar, 1);
	
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		jugador2.setYlimit();
		
		str[8] = '1';
		str[19] = (char)puntos1 + '0';
		for(int j = 0; j <= strlen(str); j++)
			write(fd, &str[j], 1);
		write(fd, &bar, 1);
	}

	if(puntos1 >= 3)
	{
		fin[8] = '1';
		for(int j = 0; j <= strlen(fin); j++)
			write(fd, &fin[j], 1);
		write(fd, &bar, 1);
		write(fd, &bar, 1);
		write(fd, &bar, 1);
		
		puntos1 = puntos2 = 0;
	}	
	else if(puntos2 >= 3)
	{
		fin[8] = '2';
		for(int j = 0; j <= strlen(fin); j++)
			write(fd, &fin[j], 1);
		write(fd, &bar, 1);
		write(fd, &bar, 1);
		write(fd, &bar, 1);
		
		puntos1 = puntos2 = 0;
	}
	
	sprintf(mensaje_sc, "%f %f %f %f %f %f %f %f %f %f %d %d",
	esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1,
	jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1,
	jugador2.x2, jugador2.y2, puntos1, puntos2);
	
	write(fd_sc, &mensaje_sc, strlen(mensaje_sc));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

}

void CMundo::Init()
{

	Plano p;
	
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	// TUBERIA LOGGER
	fd = open(tuberia, O_WRONLY | O_CREAT);
	
	if (fd == -1){
		perror("Error al abrir COM para escritura");
		exit(-1);
	}	
	
	// TUBERIA SERV-CLIENT
	
	fd_sc = open(servcli, O_WRONLY | O_CREAT);
	if (fd_sc == -1) {
		perror("Error al abrir SERVCLI para escritura");
		exit(-1);
	}
	
	// TUBERIA CLIENT-SERV
	
	fd_cs = open(cliserv, O_RDONLY);
	if (fd_cs == -1) {
		perror("Error al abrir CLISERV para lectura");
		exit(-1);
	}
	
	//CREACION DEL THREAD
	
	pthread_create(&thid1, NULL, hilo_comandos, this);
}

void* hilo_comandos(void* d)
{
	CMundo* p = (CMundo*) d;
	p->RecibeComandos();
	return p;
}

void CMundo::RecibeComandos()
{
	while(1) {
		usleep(10);
		char cad[100];
		read(fd_cs, cad, sizeof(cad));
		unsigned char key;
		sscanf(cad, "%c", &key);
		
		switch(key)
		{

		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;

		}
		
	}
}

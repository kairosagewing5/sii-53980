#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main() {

	DatosMemCompartida *sh;
	struct stat Data;
	bool running = true;
	int contador = 0;
	float check_x = 0.0;
	float check_y = 0.0;
	
	int fd = open("src/myBot", O_RDWR);
	if (fd == -1)
	{
		perror("Error al abrir myBot");
		return 1;
	}
	
	
	fstat(fd, &Data);
	sh = (DatosMemCompartida*)mmap(NULL, Data.st_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
	
	sh->accion = 1;
	
	close(fd);
	
	while(running) {
	
		usleep(25000);
		if(sh->raqueta1.y1 > sh->esfera.centro.y + sh->esfera.radio) 
			sh->accion = -1;			
		else if(sh->raqueta1.y2 < sh->esfera.centro.y - sh->esfera.radio) 
			sh->accion = 1;
		else
			sh->accion = 0;
		
		if (sh->esfera.centro.y == check_y &&
				sh->esfera.centro.y == check_y)
			contador++;
		else
			contador = 0;
			
		if (contador == 100)
			running = false;
			
		check_y = sh->esfera.centro.y;
		check_x = sh->esfera.centro.x;
				
	}

	munmap(sh, Data.st_size);	
	
}

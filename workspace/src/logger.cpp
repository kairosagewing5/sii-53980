#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int fd;
	char ch = 'a';
	
	if (mkfifo("/tmp/COM", 0666) != 0) {
		perror("Error en la creacion de la Fifo COM");
		return 1;
	}
	
	fd = open("/tmp/COM", O_RDONLY);
	if (fd == -1) {
		perror("Error");
		return 1;
	}
	
	while (read(fd, &ch, 1) == 1)
		write(1, &ch, 1);
			
	close(fd);
	unlink("/tmp/COM");
	return 0;
}
